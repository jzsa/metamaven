FROM openjdk:8-jre-alpine
COPY ./target/FileRedistributor-0.0.1-SNAPSHOT.jar /usr/src/temp/
WORKDIR /usr/src/temp
EXPOSE 8080
CMD ["java", "-jar", "FileRedistributor-0.0.1-SNAPSHOT.jar"]

