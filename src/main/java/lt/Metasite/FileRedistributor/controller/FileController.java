package lt.Metasite.FileRedistributor.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lt.Metasite.FileRedistributor.model.TaskManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@Api(value = "file")
@RequestMapping(value = "/")
public class FileController {

    private static Logger logger = LogManager.getLogger(FileController.class);
    @Autowired
    private TaskManager taskManager;

    @CrossOrigin
    @ApiOperation(value = "Initiate operation")
    @GetMapping(value = "/start")
    @ResponseStatus(value = HttpStatus.OK)
    public void start() throws InterruptedException {
        if (taskManager.canBegin())
            taskManager.start();
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/upload")
    public List<String> singleFileUpload(/*@RequestParam("file")*/ MultipartFile file,
                                                                   RedirectAttributes redirectAttributes) {

//        String fileName = file.getOriginalFilename().split("\\.")[0];
        String fileName = file.getOriginalFilename();

        if(taskManager.containsFile(fileName)){
            logger.info("file already exists");
            redirectAttributes.addFlashAttribute("message", "File already exists");
            return taskManager.getFileList();
        }

        if(file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return taskManager.getFileList();
        }

        try {

            Path temp = Files.createTempFile(file.getOriginalFilename()/*.split("\\.")[0]*/, ".tmp");

            logger.info(temp.toAbsolutePath());

            byte[] bytes = file.getBytes();

            Files.write(temp, bytes);

            taskManager.addFile(temp.toFile());

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return taskManager.getFileList();
    }

    @CrossOrigin("*")
    @ApiOperation(value = "Get file list")
    @GetMapping(value = "/list")
    @ResponseStatus(value = HttpStatus.OK)
    public List<String> fileList() throws IOException {
        return taskManager.getFileList();
    }

    @CrossOrigin("*")
    @ApiOperation(value = "Clear files")
    @GetMapping(value = "/clear")
    @ResponseStatus(value = HttpStatus.OK)
    public List<String> clear() throws IOException {
        taskManager.clear();
        return taskManager.getFileList();
    }

    @CrossOrigin("*")
    @ApiOperation(value = "Download files")
    @GetMapping(value = "/download")
    @ResponseStatus(value = HttpStatus.OK)
    public void download(HttpServletResponse response) throws IOException {
        File zip = taskManager.addToZip();
        response.setHeader("Content-Disposition", "attachment; filename=output.zip");
        response.setContentType("application/zip");
        response.setContentLength(Files.readAllBytes(Paths.get(zip.toString())).length);
        response.setHeader("Content-Type", "application/zip");
        InputStream stream = new BufferedInputStream(new FileInputStream(zip));
        FileCopyUtils.copy(stream, response.getOutputStream());
    }

}
