package lt.Metasite.FileRedistributor;

import lt.Metasite.FileRedistributor.controller.FileController;
import lt.Metasite.FileRedistributor.model.TaskManager;
import lt.Metasite.FileRedistributor.model.TasksConsumer;
import lt.Metasite.FileRedistributor.model.TasksProducer;
import org.apache.coyote.http11.AbstractHttp11Protocol;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.Environment;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.LinkedList;


@EnableSwagger2
@SpringBootApplication
public class FileRedistributorApplication extends SpringBootServletInitializer implements WebMvcConfigurer {
    private static int consumerCount = 0;

//	NOT WORKING!!!
//  @Value("#{'${file.name}'}")
//	private static String consumerArgs;

    Logger logger = LogManager.getLogger(FileRedistributorApplication.class);

    private int maxUploadSizeInMb = 20 * 1024 * 1024; // 10 MB

    public static void main(String[] args) {
        SpringApplication.run(FileRedistributorApplication.class, args);
    }

    @Autowired
    private Environment env;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(FileRedistributorApplication.class);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/*")
                .addResourceLocations("file:/resources/public/*","file:/classes/public/*");
    }

    @Bean
    public Docket swaggerDocket() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
                .apis(RequestHandlerSelectors.basePackage("lt.Metasite.FileRedistributor")).build();
//		docket.ignoredParameterTypes(Pageable.class);
        return docket;
    }

    /**
     * Swagger builder.
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("FileRedistributor documentation").version("0.1-SNAPSHOT").build();
    }

    @Bean
    @Scope(value = "prototype",
            proxyMode = ScopedProxyMode.TARGET_CLASS)
    public synchronized TasksProducer tasksProducer(TaskManager taskManager) {
        return new TasksProducer(taskManager.getListQueues(), taskManager.filePop());
    }

    @Bean
    @Scope(value = "prototype",
            proxyMode = ScopedProxyMode.TARGET_CLASS)
    public synchronized TasksConsumer tasksConsumer(TaskManager taskManager) {

        logger.info("creating bean");
        logger.info("bean count: "+consumerCount);
        int jobNo = consumerCount % 4;
        logger.info("job No: " + jobNo);
        TasksConsumer task = new TasksConsumer(taskManager.queuePop(), taskManager.getFile(jobNo));
        consumerCount++;

        return task;
    }

//	//Tomcat large file upload connection reset
//	//http://www.mkyong.com/spring/spring-file-upload-and-connection-reset-issue/
//	@Bean
//	public TomcatEmbeddedServletContainerFactory tomcatEmbedded() {
//
//		TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();
//
//		tomcat.addConnectorCustomizers((TomcatConnectorCustomizer) connector -> {
//			if ((connector.getProtocolHandler() instanceof AbstractHttp11Protocol<?>)) {
//				//-1 means unlimited
//				((AbstractHttp11Protocol<?>) connector.getProtocolHandler()).setMaxSwallowSize(-1);
//			}
//		});
//		return tomcat;
//	}

}

