package lt.Metasite.FileRedistributor.model;

import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class TaskManager {

    private static Logger logger = LogManager.getLogger(TaskManager.class);

    @Getter
    private final LinkedList<LinkedBlockingQueue<Map<String, Long>>> listQueues = new LinkedList<>();

    @Getter
    private LinkedList<File> files = new LinkedList<>();

    @Value("#{ '${file.path}' != null? '${file.path}':''}")
    private String path;

    @Value("#{'${file.name}'}")
    private String[] outputFileNames;

    @Value("#{'${file.output}'}")
    private String outputZip;

    @Autowired
    private TasksProducer tasksProducer;
    @Autowired
    private TasksConsumer tasksConsumer;


    @PostConstruct
    public void init()
    {
        for(int i=0; i < 4; i++)
        {
            listQueues.add(new LinkedBlockingQueue<>());
        }
        logger.info(Arrays.toString(outputFileNames));
    }

    /**
     * Starts sorting;
     * @throws InterruptedException - if thread interrupted;
     */
    public void start() throws InterruptedException {
        logger.info(files);

        List<Thread> producers = new ArrayList();
        List<Thread> consumers = new ArrayList();

        //First consumers to wait for jobs
        //Create and start
        for(int i = 0; i < 4; i++)
        {
            Thread t = new Thread(tasksConsumer);
            t.start();
            consumers.add(t);
        }

        for(int i = 0; i < files.size(); i++)
        {
            Thread t = new Thread(tasksProducer);
            t.start();
            producers.add(t);
        }

        logger.info("Producers started");

        for(Thread thread : producers)
            thread.join();

        logger.info("All producers finished");

        //Synchronize consumers to stop processing queue
        listQueues.forEach(queue->putWithException(queue,getExitJob()));

        //Cleanup files
        files.forEach(File::delete);

        //Make ne file list
        files = new LinkedList<>();

        for(Thread thread : consumers)
            thread.join();

        logger.info("TASK FINISHED!!");
    }

    public File addToZip(){
        try(ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(path+ outputZip))){
            for(String file : outputFileNames)
            {
                ZipEntry ze = new ZipEntry(file);
                zout.putNextEntry(ze);
                byte [] bytes = Files.readAllBytes(Paths.get(path+file));
                zout.write(bytes,0,bytes.length);
                zout.closeEntry();
            }
        }catch(Exception e)
        {
            logger.error(e.getMessage());
        }
        return new File(path+ outputZip);
    }

    public synchronized void addFile(File file){
        this.files.add(file);
    }

    public void putWithException(LinkedBlockingQueue<Map<String, Long>> queue, Map<String, Long> value){
        try{
            logger.info(value);
            queue.put(value);
            Thread.sleep(10);
        }catch (InterruptedException e)
        {
            logger.error("Who dared to interrupt me!!");
        }
    }

    /**
     * Used for automatically assigning queues to taskConsumers;
     * Invoked at construction of task consumer;
     * @return - blocking queue which is used for certain letter range;
     */
    public synchronized LinkedBlockingQueue<Map<String, Long>> queuePop(){
        LinkedBlockingQueue<Map<String, Long>> temp = listQueues.pop();
        listQueues.addLast(temp);
        return temp;
    }

    /**
     * Used for automatically assigning files to taskConsumers;
     * Invoked at construction of task producer;
     * @return - file to ride;
     */

    public synchronized File filePop(){
        File temp = files.pop();
        files.addLast(temp);
        return temp;
    }

    public String getFile(int i){
        return outputFileNames[i];
    }

    /**
     * Generates exit job for Task Consumers.
     * @return - exit job;
     */
    private Map<String,Long> getExitJob()
    {
        Map<String,Long> exitJob = new HashMap<String, Long>();
        exitJob.put("a",Long.valueOf(-1));
        return exitJob;
    }

    /**
     * Checks weather file was uploaded;
     * @param file - file to compare with;
     * @return - result;
     */
    public boolean containsFile(String file)
    {
        for(File f :files)
            if(f.getName().contains(file))
                return true;
        return false;
    }

    public void clear(){
        files = new LinkedList<>();
    }

    /**
     * Checks weather any files were uploaded for processing;
     * @return - result;
     */
    public boolean canBegin(){
        return !files.isEmpty();
    }

    public List<String> getFileList()
    {
        return files.stream()
                .map(file->file.getName().split("(\\d)+(.tmp)")[0])
                .collect(Collectors.toList());
    }

}
