package lt.Metasite.FileRedistributor.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lt.Metasite.FileRedistributor.controller.FileController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Stream;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.groupingByConcurrent;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.counting;

public class TasksProducer implements Runnable {

    private static Logger logger = LogManager.getLogger(TasksProducer.class);
    private Map<Character, Map<String, Long>> map = new HashMap<>();

    @Getter
    @Setter
    private File fileToRead;

    private final LinkedList<LinkedBlockingQueue<Map<String, Long>>> listQueues;

    public TasksProducer(LinkedList<LinkedBlockingQueue<Map<String, Long>>> listQueues, File fileToRead) {
        this.fileToRead = fileToRead;
        this.listQueues = listQueues;
    }

    @PostConstruct
    public void init() {
//        logger.info("Bean created");
        logger.info("File to read: " + fileToRead);
    }

    @PreDestroy
    public void cleanUp() {
        logger.debug("Bean destroyed");
    }

    @Override
    public void run() {
        readAllText();
        writeToQueue();
    }

    /**
     * Reads all text from file. Groups all words according starting letter;
     * Counts word repetition rate to map.
     * */
    public void readAllText() {
//        logger.info(fileToRead);
        try (Stream<String> fin = Files.lines(fileToRead.toPath())) {

            map = fin
                    .parallel()
                    .map(line -> line.split("(\\W+)"))
                    .flatMap(Arrays::stream)
                    //Eliminate empty strings, from reading new line
                    .filter(s -> !s.isEmpty())
                    .map(String::toLowerCase)
                    .collect(
                            groupingByConcurrent((String word) -> word.charAt(0),
                                    groupingBy(identity(), counting())
                            ));
            map.forEach((key, value) -> logger.info("key: " + key + " value:" + value));
        } catch (FileNotFoundException e) {
            logger.error("File: " + fileToRead.getAbsolutePath() + " not found");
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        fileToRead.delete();
    }


    /**
     * Puts word Maps according their starting letter
     * to corresponding blocking queue.
     */
    public void writeToQueue() {
        map.forEach((key, value) -> putWithException(chooseQueue(key), value));
    }

    /**
     * Method to prevent catching exceptions in lambda expression.
     * @param queue
     * @param value
     */
    private void putWithException(LinkedBlockingQueue<Map<String, Long>> queue, Map<String, Long> value) {
        try {
            logger.debug(value);
            queue.put(value);
        } catch (InterruptedException e) {
            logger.error("Who dared to interrupt me!!");
            return;
        }
    }

    /**
     * Method that chooses blocking queue, which
     * works with certain words;
     * @param key - word starting letter;
     * @return - blocking queue;
     */
    private LinkedBlockingQueue<Map<String, Long>> chooseQueue(Character key) {
        if (key <= 'g') {
//            logger.info("A-G");
            return listQueues.get(0);
        } else if (key <= 'n') {
//            logger.info("H-N");
            return listQueues.get(1);
        } else if (key <= 'u') {
//            logger.info("O-U");
            return listQueues.get(2);
        }
//        logger.info("V-Z");
        return listQueues.get(3);
    }

}
