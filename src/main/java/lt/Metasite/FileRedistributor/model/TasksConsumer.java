package lt.Metasite.FileRedistributor.model;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.*;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingLong;
import static java.util.stream.Collectors.mapping;

public class TasksConsumer implements Runnable {

    private static Logger logger = LogManager.getLogger(TasksConsumer.class);
    private static final String csvSeparator = ",";

    @Value("#{ '${file.path}' != null? '${file.path}':''}")
    private String path;
    private File temp;
    private String fileName;
    private final LinkedBlockingQueue<Map<String, Long>> queue;


    public TasksConsumer(LinkedBlockingQueue<Map<String, Long>> queue, String fileName) {
        this.queue = queue;
        this.fileName = fileName;
    }

    @PostConstruct
    public void init() {
        logger.info("Started");
//        logger.info("File path: "+path);
        logger.info("File received: " + path + fileName);
    }

    @PreDestroy
    public void cleanUp() {
        logger.info("Destroyed");
    }

    @Override
    public void run() {
        readFromQueueToTemp();
        writeTempToFile();
    }


    //Next time
    //https://github.com/aol/cyclops-rect

    /**
     * Reads word maps from queue and writes directly to temp file.
     */

    public void readFromQueueToTemp() {
        try {
            temp = File.createTempFile(Thread.currentThread().getName(), ".temp");
        } catch (IOException e) {
            logger.error("Could not create temp file");
            return;
        }

        try (BufferedWriter fin = new BufferedWriter(new FileWriter(temp))) {
            Map<String, Long> map ;
            while (true) {
                map = queue.take();
                //Close condition
                if (map.values().contains(-1L)) {
                    logger.info("willing to stop reading");
                    break;
                }

                map.entrySet().forEach(entry -> {
                    try {
                        fin.append(entry.getKey())
                                .append(csvSeparator)
                                .append(entry.getValue().toString())
                                .append(System.lineSeparator());
                    } catch (IOException e) {
                        logger.error("Whoops!!: " + e.getMessage());
                    }
                });
            }
            fin.flush();
        } catch (InterruptedException e) {
            logger.error("Who dared to interrupt me!!");
            return;
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * After all files are read. This method is called, to count all
     * word count maps from different input files.
     */

    public void writeTempToFile() {
        logger.info("writeTempToFile");

        File file = new File(path + fileName);

        //Try creating file
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
//            return null;
        }

        //Open two streams
        try (Stream<String> fin = new BufferedReader(new FileReader(temp)).lines();
             BufferedWriter fout = new BufferedWriter(new FileWriter(file))) {

            Map<String, Long> map = fin
                    .map(Entry::new)
//                    .sorted(Map.Entry.comparingByKey())
                    .collect(
                            groupingBy(Entry::getKey, summingLong(Entry::getValue)));

            map = map.entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByKey())
                    .collect(Collectors.toMap(
                            Map.Entry::getKey,Map.Entry::getValue,
                            (oldValue,newValue)->oldValue, TreeMap::new
                    ));

            map.forEach((key, value) -> {
                try {
//                    logger.info("[" + "key=" + key + "," + "value=" + value + "]");
                    fout.append(key)
                            .append(csvSeparator)
                            .append(value.toString())
                            .append(System.lineSeparator());
                } catch (IOException e) {
                    logger.error("Whoops!!: " + e.getMessage());
                }
            });

            fout.flush();
            temp.delete();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
//        return file;
    }

    private static class Entry implements Map.Entry<String, Long> {
        private final String key;
        private Long value;

        public Entry(String line) {
            String[] array = line.split(csvSeparator);
            this.key = array[0];
            this.value = Long.valueOf(array[1]);
        }

        @Override
        public String getKey() {
            return this.key;
        }

        @Override
        public Long getValue() {
            return this.value;
        }

        @Override
        public Long setValue(Long value) {
            return this.value = value;
        }
    }
}
